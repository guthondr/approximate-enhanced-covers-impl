/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <cassert>
#include "HammingRelaxedEnhancedCoverAutomaton.hh"

#define MAX(a,b) (((a)>(b))?(a):(b))

using namespace std;

HammingRelaxedEnhancedCoverAutomaton::HammingRelaxedEnhancedCoverAutomaton(const NondeterministicHammingSuffixAutomaton & nhsa) : EnhancedCoverAutomaton(nhsa.getT(), nhsa.getK()) {
	this->nhsa = &nhsa;
	DeterministicState q0(0, m_text.size());
	q0.m_dSubset.push_back(nhsa.getInitialState());
	ProcessState(&q0, 1);
}

void HammingRelaxedEnhancedCoverAutomaton::ProcessState(const pDetState qi, const unsigned dSubSize) {
	for (Alphabet::const_iterator itA = A.begin(); itA != A.end(); itA++) {
		pDetState qj = new DeterministicState(qi->getDepth() + 1, qi->m_dSubset.size());
		bool final = false;
		#ifdef RESTRICTED
		pcNondState exactElement = 0;
		bool containsExact = false;
		#endif
		for (ListOfStates::const_iterator itD = qi->m_dSubset.begin(); itD != qi->m_dSubset.end(); itD++) {
			if (*itD == nhsa->getInitialState()) {
				NondeterministicHammingSuffixAutomaton::pSetOfStates targets = nhsa->getTargetFromInitial(*itA);
				qj->m_dSubset.assign(targets->begin(), targets->end());
				if (nhsa->containsFinal(*targets))
					final = true;
				containsExact = true;
			}
			else {
				pcNondState t = nhsa->getTargetFromNonInitial(*itD, *itA);
				if (t != 0) {
					qj->m_dSubset.push_back(t);
					if (nhsa->isFinal(t))
						final = true;
					#ifdef RESTRICTED
					if (t->getLevel() == 0) {
						exactElement = t;
						containsExact = true;
					}
					#endif
				}
			}
		}
		this->peakNoStates = MAX(qj->getDepth(), this->peakNoStates);
		this->peakNoDsubElms = MAX(this->peakNoDsubElms, dSubSize+qj->m_dSubset.size());
		this->dsubsExamined++;
		if (qj->m_dSubset.size() > 1 && qj->getDepth() == qj->m_dSubset.front()->getDepth()) 
			#ifdef RESTRICTED
			if (containsExact) 
			#endif
			{
				maxfactor.push_back(*itA);
				assert(qj->getDepth() == maxfactor.length());
				if (final && qj->getDepth() > nhsa->getK()) {
// 						OUTPUT_LOG("computing covered for " << maxfactor << endl);
						size_t covered = distEnhCov(qj);
						if (covered > m_maximum_covered) {
							m_maximum_covered = covered;
#ifdef RESTRICTED
							m_enhancedCovers_length.clear();
							m_enhancedCovers_endpos.clear();
#else
							m_enhancedCovers.clear();
#endif
						}
						if (covered == m_maximum_covered) {
// 							OUTPUT_LOG("adding " << m_maximum_covered << " " << maxfactor << " covering " << m_maximum_covered  << endl);
#ifdef RESTRICTED
							m_enhancedCovers_length.push_back(maxfactor.size());
							m_enhancedCovers_endpos.push_back(exactElement->getDepth());
#else
							m_enhancedCovers.push_back(maxfactor);
#endif
						}
				}
				ProcessState(qj, dSubSize+qj->m_dSubset.size());
				maxfactor.erase(maxfactor.length() - 1);
			}
		delete qj; 
	}
}

