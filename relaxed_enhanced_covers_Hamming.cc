/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>

#include "NondeterministicHammingSuffixAutomaton.hh"
#include "HammingRelaxedEnhancedCoverAutomaton.hh"

using namespace std;

int main(int argc, char** argv) {
	const bool experiments = (argc == 4);
	if (argc != 4 && argc != 2) {
		cerr << "USAGE:" << endl;
		cerr << "experiments (output statistics only): <input file> <length> <k>" << endl;
		cerr << "output results (input from STDIN): <k>" << endl;
		return EXIT_FAILURE;
	}
	char *input;
	if (experiments) {
		ifstream input_file(argv[1]);
		unsigned input_length = atoi(argv[2]);
		input = new char[input_length];
		input_file.read(input, input_length);
		input_file.close();
	} else {
		istreambuf_iterator<char> begin(cin), end;
		const string s(begin, end);
		input = new char[s.size()+1];
		strcpy (input, s.c_str());
		
	}
	const unsigned k = atoi(experiments ? argv[3] : argv[1]);
	if (k <= 0) {
		cerr << "k must be positive" << endl;
		return EXIT_FAILURE;
	}
	const NondeterministicHammingSuffixAutomaton nha(input, k);
	const HammingRelaxedEnhancedCoverAutomaton dhcca(nha);
#ifdef RESTRICTED
	list<size_t>::const_iterator itL = dhcca.m_enhancedCovers_length.begin(); list<size_t>::const_iterator itP = dhcca.m_enhancedCovers_endpos.begin();
	for (; itL != dhcca.m_enhancedCovers_length.end() && itP != dhcca.m_enhancedCovers_endpos.end(); itL++, itP++) {
		if (!experiments) 
			printf("%.*s\n", (int)*itL, input + *itP-*itL);
	}
#else
	for (list<string>::const_iterator itL = dhcca.m_enhancedCovers.begin(); itL != dhcca.m_enhancedCovers.end(); itL++) {
		if (!experiments)
			cout << *itL << endl;
	}
#endif

	delete [] input;
	input = 0;
	
	if (experiments) {
		cout << dhcca.getNoCovers() << " " << dhcca.getPeakNoDsubElms() << " " << dhcca.getDsubsExamined() << " " << dhcca.getPeakNoStates();
	}
	
	return (EXIT_SUCCESS);
}
