This is implementation of algorithms for computing all k-approximate enhanced covers and all relaxed k-approximate enhanced covers of given string.

It is licensed under GPLv3 (see file LICENSE.txt).

This program is written in C++98 and does not require any non-standard library to compile or run. To compile, it is recommended to use gcc version 4 (or newer) and GNU Make.

This software consists of two programs:
covers_hamming: implementation of algorithm for computing all k-approximate enhanced covers
relaxed_covers_hamming: implementation of algorithm for computing all relaxed k-approximate enhanced covers

To compile them, run:
make ../dist/covers_hamming
and
make ../dist/relaxed_covers_hamming

The programs may be run in two modes.

In results mode (relaxed_covers_hamming only), the given string (i.e., the string of which to compute covers) is read from standard input, found (relaxed) k-approximate covers are reported to standard output. Some debugging info may be sent to error output. Program should be invoked as:
relaxed_covers_hamming <k>

In experiments mode, the given string is read from given file, some statistics are reported to standard output: number of found covers, peak number of d-subset elements stored in memory at a time, total number of examined d-subset elements, peak number of states stored in memory at a time. Program should be invoked as:
<binary_of_program> <input_file_path> <input_length> <k>

