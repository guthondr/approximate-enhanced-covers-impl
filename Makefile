#  Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
#  Faculty of Information Technology, Czech Technical University in Prague, 2017
#  
#  This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 

CXX = g++ 
EXPFLAGS = -DNDEBUG -pipe -pedantic -ansi -Wall -Wextra -Werror -O3 -fomit-frame-pointer -s
DEBUGFLAGS = -DDEBUG -ggdb -Og -rdynamic -pipe -pedantic -ansi -Wall -Wextra -Werror
CXXMFLAGS = -march=native
OBJECTDIR = ../build
DIST_DIR = ../dist
DEBUG_DIR = ../debug
MKDIR = mkdir --parent
RM = rm --recursive --force

OBJECTFILES_EXP_c= \
	${OBJECTDIR}/enhanced_covers_Hamming-exp.o \
	${OBJECTDIR}/HammingEnhancedCoverAutomaton-exp.o
	
OBJECTFILES_DEBUG_c= \
	${OBJECTDIR}/enhanced_covers_Hamming-debug.o \
	${OBJECTDIR}/HammingEnhancedCoverAutomaton-debug.o
	
OBJECTFILES_EXP_r= \
	${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-exp.o\
	${OBJECTDIR}/relaxed_enhanced_covers_Hamming-exp.o\
	${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-exp.o\

OBJECTFILES_DEBUG_r= \
	${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-debug.o\
	${OBJECTDIR}/relaxed_enhanced_covers_Hamming-debug.o\
	${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-debug.o\

OBJECTFILES_EXP_g= \
	${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-exp.o\
	${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-exp.o\
	${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-exp.o\

OBJECTFILES_DEBUG_g= \
	${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-debug.o\
	${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-debug.o\
	${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-debug.o\

${DIST_DIR}/covers_hamming : ${OBJECTFILES_EXP_c}
	${MKDIR} ${DIST_DIR}
	$(CXX) ${EXPFLAGS} -o ${DIST_DIR}/covers_hamming ${OBJECTFILES_EXP_c}

${DEBUG_DIR}/covers_hamming : ${OBJECTFILES_DEBUG_c}
	${MKDIR} ${DEBUG_DIR}
	$(CXX) ${DEBUGFLAGS} -o ${DEBUG_DIR}/covers_hamming ${OBJECTFILES_DEBUG_c}

${DIST_DIR}/relaxed_covers_hamming : ${OBJECTFILES_EXP_r}
	${MKDIR} ${DIST_DIR}
	$(CXX) ${EXPFLAGS} -DRESTRICTED -o ${DIST_DIR}/relaxed_covers_hamming ${OBJECTFILES_EXP_r}

${DEBUG_DIR}/relaxed_covers_hamming : ${OBJECTFILES_DEBUG_r}
	${MKDIR} ${DEBUG_DIR}
	$(CXX) ${DEBUGFLAGS} -DRESTRICTED -o ${DEBUG_DIR}/relaxed_covers_hamming ${OBJECTFILES_DEBUG_r}

${DIST_DIR}/general_relaxed_covers_hamming : ${OBJECTFILES_EXP_g}
	${MKDIR} ${DIST_DIR}
	$(CXX) ${EXPFLAGS} -o ${DIST_DIR}/general_relaxed_covers_hamming ${OBJECTFILES_EXP_g}

${DEBUG_DIR}/general_relaxed_covers_hamming : ${OBJECTFILES_DEBUG_g}
	${MKDIR} ${DEBUG_DIR}
	$(CXX) ${DEBUGFLAGS} -o ${DEBUG_DIR}/general_relaxed_covers_hamming ${OBJECTFILES_DEBUG_g}

${OBJECTDIR}/enhanced_covers_Hamming-exp.o : enhanced_covers_Hamming.cc HammingEnhancedCoverAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/enhanced_covers_Hamming-exp.o enhanced_covers_Hamming.cc

${OBJECTDIR}/enhanced_covers_Hamming-debug.o : enhanced_covers_Hamming.cc HammingEnhancedCoverAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/enhanced_covers_Hamming-debug.o enhanced_covers_Hamming.cc

${OBJECTDIR}/HammingEnhancedCoverAutomaton-exp.o : HammingEnhancedCoverAutomaton.cc HammingEnhancedCoverAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/HammingEnhancedCoverAutomaton-exp.o HammingEnhancedCoverAutomaton.cc

${OBJECTDIR}/HammingEnhancedCoverAutomaton-debug.o : HammingEnhancedCoverAutomaton.cc HammingEnhancedCoverAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/HammingEnhancedCoverAutomaton-debug.o HammingEnhancedCoverAutomaton.cc

${OBJECTDIR}/relaxed_enhanced_covers_Hamming-exp.o : relaxed_enhanced_covers_Hamming.cc NondeterministicHammingSuffixAutomaton.hh HammingRelaxedEnhancedCoverAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/relaxed_enhanced_covers_Hamming-exp.o relaxed_enhanced_covers_Hamming.cc
	
${OBJECTDIR}/relaxed_enhanced_covers_Hamming-debug.o : relaxed_enhanced_covers_Hamming.cc NondeterministicHammingSuffixAutomaton.hh HammingRelaxedEnhancedCoverAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/relaxed_enhanced_covers_Hamming-debug.o relaxed_enhanced_covers_Hamming.cc
	
${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-exp.o : HammingRelaxedEnhancedCoverAutomaton.cc HammingRelaxedEnhancedCoverAutomaton.hh State.hh  Automaton.hh EnhancedCoverAutomaton.hh NondeterministicHammingSuffixAutomaton.hh NondeterministicHammingSuffixAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-exp.o HammingRelaxedEnhancedCoverAutomaton.cc

${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-debug.o : HammingRelaxedEnhancedCoverAutomaton.cc HammingRelaxedEnhancedCoverAutomaton.hh State.hh  Automaton.hh EnhancedCoverAutomaton.hh NondeterministicHammingSuffixAutomaton.hh NondeterministicHammingSuffixAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton-debug.o HammingRelaxedEnhancedCoverAutomaton.cc

${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-exp.o : NondeterministicHammingSuffixAutomaton.cc NondeterministicHammingSuffixAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-exp.o NondeterministicHammingSuffixAutomaton.cc
	
${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-debug.o : NondeterministicHammingSuffixAutomaton.cc NondeterministicHammingSuffixAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -DRESTRICTED -o ${OBJECTDIR}/NondeterministicHammingSuffixAutomaton-debug.o NondeterministicHammingSuffixAutomaton.cc
	
${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-exp.o : relaxed_enhanced_covers_Hamming.cc NondeterministicHammingSuffixAutomaton.hh HammingRelaxedEnhancedCoverAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-debug.o relaxed_enhanced_covers_Hamming.cc
	
${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-debug.o : relaxed_enhanced_covers_Hamming.cc NondeterministicHammingSuffixAutomaton.hh HammingRelaxedEnhancedCoverAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/general_relaxed_enhanced_covers_Hamming-debug.o relaxed_enhanced_covers_Hamming.cc
	
${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-exp.o : HammingRelaxedEnhancedCoverAutomaton.cc HammingRelaxedEnhancedCoverAutomaton.hh State.hh  Automaton.hh EnhancedCoverAutomaton.hh NondeterministicHammingSuffixAutomaton.hh NondeterministicHammingSuffixAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-exp.o HammingRelaxedEnhancedCoverAutomaton.cc

${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-debug.o : HammingRelaxedEnhancedCoverAutomaton.cc HammingRelaxedEnhancedCoverAutomaton.hh State.hh  Automaton.hh EnhancedCoverAutomaton.hh NondeterministicHammingSuffixAutomaton.hh NondeterministicHammingSuffixAutomaton.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/HammingRelaxedEnhancedCoverAutomaton_general-debug.o HammingRelaxedEnhancedCoverAutomaton.cc

${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-exp.o : NondeterministicHammingSuffixAutomaton.cc NondeterministicHammingSuffixAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${EXPFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-exp.o NondeterministicHammingSuffixAutomaton.cc
	
${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-debug.o : NondeterministicHammingSuffixAutomaton.cc NondeterministicHammingSuffixAutomaton.hh State.hh Automaton.hh NondeterministicState.hh
	${MKDIR} ${OBJECTDIR}
	$(CXX) ${DEBUGFLAGS} ${CXXMFLAGS} -c -o ${OBJECTDIR}/NondeterministicHammingSuffixAutomaton_general-debug.o NondeterministicHammingSuffixAutomaton.cc
	
clean:
	${RM} ${OBJECTDIR}
	${RM} ${DIST_DIR}
	${RM} ${DEBUG_DIR}
	${RM} *.gch
