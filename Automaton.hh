/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#ifndef _AUTOMATON_H
#define	_AUTOMATON_H

#include <set>
#include <iostream>

#ifdef NDEBUG
#define OUTPUT_LOG(x) ;
#else
#define OUTPUT_LOG(x) clog << x;
#endif

using namespace std;

class Automaton {
protected:
	template <class StateT>
	struct statesymbol {
		const StateT *state;
		const char symbol;
		statesymbol(const StateT *_state, const char _symbol) : state(_state), symbol(_symbol) {};
		bool operator<(const statesymbol &oth) const {
			const StateT &thisState = *(this->state), &othState = *(oth.state);
			if (thisState < othState)
				return true;
			else
				return this->symbol < oth.symbol;
		};
	};
};

typedef set<char> Alphabet;
extern Alphabet A;

#endif	/* _AUTOMATON_H */

