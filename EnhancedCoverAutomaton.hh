/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _ENHANCEDCOVERAUTOMATON_H
#define	_ENHANCEDCOVERAUTOMATON_H



#include <string>
#include <list>
#include <vector>
#include "Automaton.hh"
#include "NondeterministicState.hh"

typedef const NondeterministicState* pcNondState;
typedef std::vector<pcNondState> ListOfStates;

struct DeterministicState : public State {
	
    ListOfStates m_dSubset;
    
    DeterministicState(const unsigned _depth, const size_t _maxSize) : State(_depth) {
        m_dSubset.reserve(_maxSize);
    };
    
    ~DeterministicState() {
//         for (ListOfStates::const_iterator it = m_dSubset.begin(); it != m_dSubset.end(); ++it) {
//             delete *it;
//         }
        m_dSubset.clear();
    }
    
	#ifdef RESTRICTED
	bool ContainsExact() const {
		for (ListOfStates::const_iterator it = m_dSubset.begin(); it != m_dSubset.end(); it++)
			if ((*it)->getLevel() == 0)
				return true;
			return false;
	}
	
	#endif
	friend ostream& operator<<(ostream &os, const DeterministicState &state) {
		os << state.getDepth() << "{ ";
		for (ListOfStates::const_iterator it = state.m_dSubset.begin(); it != state.m_dSubset.end(); it++)
			os << (**it).getDepth() << "(" << (**it).getLevel() << ") ";
		os << "}";
		return os;
	}
	
	friend ostream& operator<<(ostream &os, const DeterministicState *state) {
		return operator<<(os, *state);
	}
};

class EnhancedCoverAutomaton : public Automaton {
protected:
    const std::string & m_text;
    const unsigned m_k;
	unsigned m_maximum_covered;
    size_t peakNoStates, dsubsExamined, peakNoDsubElms;
    
	unsigned distEnhCov(const DeterministicState * _state) const {
		pcNondState prevElm = _state->m_dSubset.front();
		const unsigned borderLength = _state->getDepth();
		unsigned result = borderLength;
		OUTPUT_LOG("computing covered for d-subset " << *_state << endl);
		for (ListOfStates::const_iterator it = _state->m_dSubset.begin(); it != _state->m_dSubset.end(); ++it) {
			const pcNondState currElm = *it;
			const unsigned dist = currElm->getDepth() - prevElm->getDepth();
			OUTPUT_LOG("increase " << result << " by " << ((dist < borderLength) ? dist : borderLength) << " curr " << currElm->getDepth() << " prev " << prevElm->getDepth() << endl);
			result += (dist < borderLength) ? dist : borderLength;
			prevElm = currElm;
		}
		return result;
	}

public:
	EnhancedCoverAutomaton(const std::string & _T, const unsigned _k) : m_text(_T), m_k(_k), m_maximum_covered(0), peakNoStates(0), dsubsExamined(0), peakNoDsubElms(0) {};
    
    unsigned getPeakNoStates() const {
        return peakNoStates;
    }
    unsigned getDsubsExamined() const {
        return dsubsExamined;
    }
    unsigned getPeakNoDsubElms() const {
        return peakNoDsubElms;
    }
};
#endif	/* _ENHANCEDCOVERAUTOMATON_H */
