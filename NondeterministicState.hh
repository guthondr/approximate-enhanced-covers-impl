/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _NONDETERMINISTICSTATE_H
#define _NONDETERMINISTICSTATE_H

#include "State.hh"

class NondeterministicState : public State {
    unsigned level;
public:
    NondeterministicState(const unsigned _depth, const unsigned _level) : State(_depth), level(_level) {};
    unsigned getLevel() const {
	return level;
    };
    bool operator<(const NondeterministicState &oth) const;
//     friend ostream& operator <<(ostream &os, const NondeterministicState &obj);
//     friend ostream& operator <<(ostream &os, const NondeterministicState *obj);
};

typedef NondeterministicState *pNondState;
typedef const NondeterministicState* pcNondState;

#endif //_NONDETERMINISTICSTATE_H
