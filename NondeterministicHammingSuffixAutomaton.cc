/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <cassert>
#include <cstring>
#include "NondeterministicHammingSuffixAutomaton.hh"

using namespace std;

Alphabet A;

NondeterministicHammingSuffixAutomaton::NondeterministicHammingSuffixAutomaton(const string &T, const unsigned k) {
	typedef pNondState* ppState;
	assert(!T.empty());
	A.insert(T.begin(), T.end());
	assert(A.size() > 0);
	assert(T.size() >= A.size());
	assert(T.size() >= k);
	this->T = T;
	this->k = k;
	Q = new ppState[T.length()+1];
	for (size_t i = 0; i < T.length()+1; i++) {
		Q[i] = new pNondState[k+1];
		memset(Q[i], 0, (k+1)*sizeof(pNondState));
	}
#ifndef NDEBUG
	for (size_t i = 0; i < T.length(); i++) {
		for (size_t l = i + 1; l <= k; l++)
			assert(Q[i][l] == 0);
	}
#endif
	for (unsigned l = 0; l <= k; l++) {
		Q[T.length()][l] = new NondeterministicState(T.length(), l);
	}
	for (unsigned i=T.length(); i > 1; i--) {
		for (unsigned lit = min(k, i) + 1; lit > 0; lit--) {
			Q[i-1][lit-1] = new NondeterministicState(i-1, lit - 1);
		}
	}
	for(Alphabet::const_iterator itA = A.begin(); itA != A.end(); itA++) {
		SetOfStates *targetStates = new SetOfStates();
		for (unsigned i=1; i <= T.length(); i++) {
			const unsigned textIndex = i - 1;
			if (T[textIndex] == *itA)
				targetStates->push_back(Q[i][0]);
			else
				targetStates->push_back(Q[i][1]);
		}
		deltaInit.insert(DeltaInitPair(*itA, targetStates));
	}
}

pcNondState NondeterministicHammingSuffixAutomaton::getTargetFromNonInitial(pcNondState state, const char symbol) const {
	assert(state != 0);
	if (state->getDepth() < T.length()) {
		if (T[state->getDepth()] == symbol)
			return Q[state->getDepth() + 1][state->getLevel()];
		else if (state->getLevel() < k)
			return Q[state->getDepth() + 1][state->getLevel() + 1];
		else
			return 0;
	}
	else
		return 0;
}

NondeterministicHammingSuffixAutomaton::pSetOfStates NondeterministicHammingSuffixAutomaton::getTargetFromInitial(const char symbol) const {
	TransitionFunctionInit::const_iterator itDelta = deltaInit.find(symbol);
	assert(itDelta != deltaInit.end());
	return itDelta->second;
}

bool NondeterministicHammingSuffixAutomaton::isFinal(pcNondState state) const {
	assert(state != 0);
	assert(!T.empty());
	return state->getDepth() == T.length();
}

bool NondeterministicHammingSuffixAutomaton::containsFinal(const SetOfStates &subset) const {
	assert(!T.empty());
	for (SetOfStates::const_iterator it = subset.begin(); it != subset.end(); it++)
		if ((*it)->getDepth() == T.length())
			return true;
		return false;
}

bool NondeterministicState::operator<(const NondeterministicState &oth) const {
	if (State::operator<(oth))
		return true;
	else
		return this->level < oth.level;
}

ostream& operator<<(ostream &os, const NondeterministicState &state) {
	os << state.getDepth() << "^" << state.getLevel();
	return os;
}

ostream& operator<<(ostream &os, const NondeterministicState *state) {
	return operator<<(os, *state);
}
