/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "HammingEnhancedCoverAutomaton.hh"
using namespace std;


int main(int argc, char** argv) {
	if (argc != 5) {
		cerr << "use with four parameters: <input file> <length> <k> <output file for result size>" << endl;
		return EXIT_FAILURE;
	}
	const unsigned input_length = atoi(argv[2]);
	OUTPUT_LOG("input length is " << input_length << endl);
	char *input = new char[input_length+1]();
	ifstream input_file(argv[1]);
	input_file.read(input, input_length);
        if (!input_file)
            return EXIT_FAILURE;
	input_file.close();
        {
            const string inputT(input);
            OUTPUT_LOG("input string is " << inputT << endl);
            const int k = atoi(argv[3]);
            OUTPUT_LOG("k is " << k << endl);
            if (k < 0) {
                    cerr << "k must be nonnegative" << endl;
                    return EXIT_FAILURE;
            }
            HammingEnhancedCoverAutomaton dhcca(inputT, k);
            const list<size_t> * covers = dhcca.computeAndReportEnhCovers();
			
#ifndef NDEBUG
			for (list<size_t>::const_iterator it = covers->begin() ; it != covers->end() ; it++) {
				cout << inputT.substr(0, *it);
            }
            cout << endl;
#endif //NDEBUG

//     #ifndef NDEBUG
//             for (list<string>::const_iterator it = covers->begin(); it != covers->end(); it++) {
// 				OUTPUT_LOG(it->cover << " : " << it->distance << endl);
// 			}
//     #endif
			
			
// experiments output
            cout << covers->size() << " " << dhcca.getPeakNoStates() << " " << dhcca.getDsubsExamined() << " " << dhcca.getPeakNoDsubElms();
// experiments output end
			
    //         delete [] input;
            delete covers;
        }
        delete [] input;
	return (EXIT_SUCCESS);
}
