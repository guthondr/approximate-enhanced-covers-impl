/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _HAMMINGRELAXEDENHANCEDCOVERAUTOMATON_H
#define	_HAMMINGRELAXEDENHANCEDCOVERAUTOMATON_H
#include <list>
#include <stack>
#include <string>

#include "State.hh"
#include "Automaton.hh"
#include "EnhancedCoverAutomaton.hh"
#include "NondeterministicHammingSuffixAutomaton.hh"

using namespace std;

#ifndef NDEBUG
struct RSDC {
    string cover;
    unsigned distance;
};
typedef list<RSDC> ListOfRSDC;
#endif

class HammingRelaxedEnhancedCoverAutomaton : public EnhancedCoverAutomaton {
    typedef DeterministicState *pDetState;
    typedef statesymbol<DeterministicState> statesymboldet;
	pDetState q0;
    void ProcessState(const pDetState state, const unsigned dSubSize);
    string maxfactor;
    const NondeterministicHammingSuffixAutomaton * nhsa;
public:
#ifdef RESTRICTED
	list<size_t> m_enhancedCovers_length;
	list<size_t> m_enhancedCovers_endpos;
#else
	list<string> m_enhancedCovers;
#endif
	HammingRelaxedEnhancedCoverAutomaton(const NondeterministicHammingSuffixAutomaton & nhsa);
	unsigned getNoCovers() const {
#ifdef RESTRICTED
		return m_enhancedCovers_length.size();
#else
		return m_enhancedCovers.size();
#endif
	}
};

#endif	/* _HAMMINGRELAXEDENHANCEDCOVERAUTOMATON_H */
