/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "HammingEnhancedCoverAutomaton.hh"

using namespace std;


const DeterministicState* HammingEnhancedCoverAutomaton::constrFirstState(void) const {
    const char t1 = m_text[0];
    DeterministicState * result = new DeterministicState(1, m_text.size());
    for (size_t i = 1; i <= m_text.size(); i++) {
        if (t1 == m_text[i-1])
            result->m_dSubset.push_back(new NondeterministicState(i, 0));
        else if (m_k >= 1)
            result->m_dSubset.push_back(new NondeterministicState(i, 1));
    }
    return result;
}

const DeterministicState* HammingEnhancedCoverAutomaton::next(const DeterministicState * _current, const unsigned _posInText) const {
    DeterministicState * result = new DeterministicState(_current->getDepth()+1, _current->m_dSubset.size());
    const char ti = m_text[_posInText-1];
    for (ListOfStates::const_iterator it = _current->m_dSubset.begin(); it != _current->m_dSubset.end(); ++it) {
        const pcNondState elm = *it;
        const unsigned currDepth = elm->getDepth();
        if (currDepth >= m_text.size())
            break;
        const unsigned newLevel = m_text[currDepth+1-1] == ti ? elm->getLevel() : elm->getLevel()+1;
        if (newLevel > m_k)
            continue;
        result->m_dSubset.push_back(new NondeterministicState(currDepth+1, newLevel));
    }
    return result;
}

const list<size_t> *  HammingEnhancedCoverAutomaton::computeAndReportEnhCovers() {
    list<size_t> * enhancedCovers = new list<size_t>();
    const DeterministicState * firstState = constrFirstState();
    peakNoStates = dsubsExamined = 1;
    peakNoDsubElms = firstState->m_dSubset.size();
	size_t prefixLength = 1;
	
	const pcNondState lastElm = firstState->m_dSubset.back();
	
	if (lastElm->getLevel() == 0 && lastElm->getDepth() == m_text.size() && prefixLength > m_k) {
        m_maximum_covered = distEnhCov(firstState);
// 		OUTPUT_LOG("prefix " << m_text.substr(0, prefixLength) << " covers " << m_maximum_covered << endl);
        enhancedCovers->push_back(prefixLength);
    }
    
    const DeterministicState * nextState = firstState;
    for (size_t i = 2; i <= m_text.size(); i++) {
        prefixLength++;
        {
            const DeterministicState * tmpNext = next(nextState, i);
            peakNoStates = dsubsExamined = 2;
            peakNoDsubElms = max(tmpNext->m_dSubset.size() + nextState->m_dSubset.size(), peakNoDsubElms);
            delete nextState;
            nextState = tmpNext;
        }
        
        if (nextState->m_dSubset.size() <= 1)
            break;
        
		const pcNondState lastElm = nextState->m_dSubset.back();
		
		if (lastElm->getDepth() == m_text.size() && lastElm->getLevel() == 0 && prefixLength > m_k) {
            const unsigned covered = distEnhCov(nextState);
// 			OUTPUT_LOG("prefix " << m_text.substr(0, prefixLength) << " covers " << covered << endl);
            if (covered > m_maximum_covered) {
				OUTPUT_LOG("new maximum:" << m_text.substr(0, prefixLength) << endl);
                m_maximum_covered = covered;
                enhancedCovers->clear();
            }
            if (covered == m_maximum_covered) {
                enhancedCovers->push_back(prefixLength);
				OUTPUT_LOG("new maximum:" << m_text.substr(0, prefixLength) << endl);
            }
        }
    }
    
    delete nextState;    
    return enhancedCovers;    
}
