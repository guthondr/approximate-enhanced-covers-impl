/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _NONDETERMINISTICAUTOMATON_H
#define	_NONDETERMINISTICAUTOMATON_H
#include <string>
#include <list>
#include <map>

#include "State.hh"
#include "Automaton.hh"
#include "NondeterministicState.hh"

using namespace std;


typedef NondeterministicState *pNondState;
class NondeterministicHammingSuffixAutomaton : public Automaton {
public:
    typedef statesymbol<NondeterministicState> statesymbnondet;
    typedef list<pcNondState> SetOfStates;
    typedef SetOfStates *pSetOfStates;
	typedef map<char, pSetOfStates> TransitionFunctionInit;
    typedef pair<char, pSetOfStates> DeltaInitPair;
private:
    unsigned k;
    string T;
    pNondState **Q;
    TransitionFunctionInit deltaInit;
public:
    NondeterministicHammingSuffixAutomaton(const string &T, const unsigned k);
    pcNondState getInitialState() const {return Q[0][0];}
    pSetOfStates getTargetFromInitial(const char symbol) const;
    pcNondState getTargetFromNonInitial(pcNondState state, const char symbol) const;
    bool isFinal(pcNondState state) const;
    bool containsFinal(const SetOfStates &subset) const;
#ifndef NDEBUG
	~NondeterministicHammingSuffixAutomaton() {
		for (size_t i = 0; i <= T.size(); i++) {
			for (size_t j = 0; j <= k; j++) {
				delete Q[i][j];
			}
			delete [] Q[i];
		}
		delete [] Q;
		for (TransitionFunctionInit::const_iterator i = deltaInit.begin(); i != deltaInit.end(); ++i) {
			delete i->second;
		}
	}
#endif
	unsigned getK() const {return k;}
    const string &getT() const {return T;}
};

#endif	/* _NONDETERMINISTICAUTOMATON_H */
