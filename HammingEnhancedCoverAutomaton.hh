/*
 * Copyright Ondrej Guth <ondrej.guth@fit.cvut.cz>
 * Faculty of Information Technology, Czech Technical University in Prague, 2017
 * 
 * This file is part of implementation of algorithm to compute all k-approximate enhanced covers.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _HAMMINGENHANCEDCOVERAUTOMATON_H
#define	_HAMMINGENHANCEDCOVERAUTOMATON_H



#include <string>
#include <list>
#include <vector>
#include "Automaton.hh"
#include "NondeterministicState.hh"
#include "EnhancedCoverAutomaton.hh"

class HammingEnhancedCoverAutomaton : public EnhancedCoverAutomaton {    
    const DeterministicState* constrFirstState(void) const;
    const DeterministicState* next(const DeterministicState * _current, const unsigned _posInText) const;

public:
	HammingEnhancedCoverAutomaton(const std::string & _T, const unsigned _k) : EnhancedCoverAutomaton(_T, _k) {};
    
    const std::list<size_t> * computeAndReportEnhCovers();
};
#endif	/* _HAMMINGENHANCEDCOVERAUTOMATON_H */
